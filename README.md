# Master Proposed Framework

Master framework for big data biomarker analysis (CNN-GAN) over 60,025 genes (245 * 245 px as a grayscale)

![3 Lung Cancer gene Biomarker identification](https://user-images.githubusercontent.com/96921111/151718988-982fe460-f26b-4333-897d-ba68b32b71a6.jpg)
<!-- <p align="left">
  <img src="https://user-images.githubusercontent.com/96921111/151718988-982fe460-f26b-4333-897d-ba68b32b71a6.jpg" width="700" />
</p> -->


## REQUIREMENTS

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install whole package are used.

```bash
pip install <pck-name>:

colorama==0.4.1
colorlog==4.0.2
pip install pandas==1.2.5
XlsxWriter
xlrd
pip install pymongo[srv]
scikit-learn
pip~=20.2.3
typing~=3.7.4.3
pathlib~=1.0.1
setuptools~=49.2.1
pymongo~=4.0.1
numpy~=1.22.0
pandas~=1.3.5
pip install opencv-python
```

## ENTRY POINT START

```bash
python main.py -c settings.json
```

## FEATURES

> #### Step 1. Data Preprocessing and cleaning 
- `Description` execution time `3 min 5 seconds` (Yup, wow and super exiting).
- All these process powered by sk-learn.
1. Data Cleaning
   1. [x] Anomaly detection or outliers 
      1. Samples with `None value` in features `missing values`
      2. `Equation` Imputer when facing any `None` value then will fill field with mean of document feature values
   2. [ ] Zeros values `Non-Function`
2. Data Scaling
   1. [x] Standardization
      1. To make sure all values of features are around of variance `origin`
      2. `Equation` z = (feature - mean) / standard deviation 
   2. [x] Normalizer (Row means -> Sample)
      1. To make sure that summation of each row is max value
      2. Normalize samples individually to unit norm.
      3. Each sample (i.e. each row of the data matrix) with at least one non zero component is rescaled independently of other samples so that its norm (l1, l2 or inf) equals one.
   3. [x] MaxAbsScaler (Column means -> Feature (gene))
      1. Scale each feature by its maximum absolute value.
      2. This estimator scales and translates each feature individually such that the maximal absolute value of each feature in the training set will be 1.0. It does not shift/center the data, and thus does not destroy any sparsity.
   4. [x] Normalization `last step`
      1. Then  fit all values in range (0-255) as a image grayscale
   5. [ ] FunctionTransformer `None Function`
   6. [ ] Binarizer `None Function`
   7. [ ] PolynomialFeatures `None Function`
3. Data Split 
   1. [x] train_test_split 
   2. [x] KFold 
   3. [x] Repeated KFold 
   4. [x] Stratified KFold 
   5. [x] Repeated Stratified KFold 
   6. [x] Leave One Out 
   7. [x] Leave P Out 
   8. [x] Shuffle Split 
   9. [x] Stratified Shuffle Split 
   10. [x] Time Series Split
> #### Step 2. Data Representation and build mongoDB collections
- All functions will be done by using ```ReadDataAndFormat()``` as instance from base class.

> #### Step 3. Data Revamping 
- `Description` execution time `0 min 10 seconds` (Yup, wow and super exiting)
- Next step should fit and revamp each sample to image (grayscale 0-255)
- Fitting can be done using opencv (cv2) and reshape by numpy `245 * 245` PXs).
- Output are show (LUAD, LUSC) samples as the following:
<p float="center">
  <img src="https://user-images.githubusercontent.com/96921111/153780833-bd8b8ac4-cdfa-4c32-b680-f9a4d82e5ccb.png" width="300" />
  <img src="https://user-images.githubusercontent.com/96921111/153780841-93113d4f-b07a-4077-9fc0-7921ad4197b0.png" width="300" /> 
</p>

> #### Step 4. Data Augmentation
##### DC-GAN on FPKM lung data after revamping to (245*245) and we will put it in (256) as a uniformal dims

##### Introduction
An important field within a machine vision is a bio-vision. However, a main problem within this area of research is that it is difficult to obtain a large sample of training images. Limitations to obtaining sequence of genes include: the low availability of participants, the time it takes to obtain and process of getting high quality data, as well as the fact that participants have to stay still for long periods of time. Therefore it is useful to implement a generative adversarial network (GAN) that can be trained on existing sequence of genes after revamping process and then if trained successfully, it can generate an infinite number of plausible sequence genes acting in an image. This would aid the training of machine vision techniques.

In particular, I have implemented a deep convolutional generative adversarial network (DCGAN) with reference DCGAN specifications.

In the DCGAN, the use of convolutional layers allows higher quality feature mapping and recognition relative to the traditional GAN which is only connected by dense layers. In my GAN implementation, I followed specifications such as:
1. Using LeakyReLU in the discriminator
2. Using strided convolutions in the discriminator and fractional-strided convolutions in the generator
3. Using batchnorm in both the generator and discriminator
4. Remove fully connected hidden layers
5. Scaling training image pixel values from -1 to 1
6. In LeakyReLU, the slope of the leak was set to 0.2
7. Using an Adam optimiser with learning rate of 0.0002 for both generator and discriminator (I used 0.0002 for the generator and 0.0001 for the discriminator)
8. No pooling layers

I also did not follow several specifications as I found they either did not work or produced lower quality results:
1. They suggested the use of a ReLU activation function in the generator, however, I found LeakyReLU worked better as they are more effective in preventing vanishing gradients. They also suggested the use of a Tanh activation function in the final layer of the generator, however, I found my model worked better without any activation functions in the generator and discriminator.
2. Instead of using a batch size of 128, I used a batch size of 10 (i.e. 10 real and 10 fake images in each batch). I found larger batch sizes would overload the GPU.
3. The paper suggested the use of beta_1=0.5 for the Adam optimiser, however I found that using the default beta_1=0.9 worked fine.
4. I decided to use a latent space of 256 instead of 100 for no real reason and this worked quite well
5. For the Conv2DTranspose layers, when using the depicted kernel size of (5,5) with stride (2,2) I got very aliased generator images with grid artifacts. This was remedied by using a kernel size of (4,4) with stride (2,2)
6. Also I tried using four fractionally-strided convolutions (Conv2DTranspose) layers with one convolutional layer after and ended up with mode collapse. My model was working and produced very high quality sequence genes images (SSIM>0.75) however, my generator would only produce the same images regardless of the noise input. I later fixed this by using three Conv2DTranspose layers and two convolutional layers instead.
7. I used dropout layers in my discriminator to make my discriminator learn more slowly. I did not try running the GAN without dropout layers so I'm not sure if this had any real effect, but the current model is quite effective.
8. In contrast to the number of filters in the generator (which had filters 1024, 512, 256, 128 for the Conv2DTranspose layers), I used a maximum of 256 for the filters in my layers. I originally implemented the same number of filters in my generator as the paper, however, I found that my GPU would run out of memory due to the large number of filters.

##### Data
Data split mechansim as the following (70% training images, 10% test images, 20% validation images). The size of these images are 256x256 and are greyscale with pixel values ranging from 0 to 255.

##### Model script

###### Generator
The generator generates 256 x 256 images and is designed to take an input noise vector with latent space of size 256. Batchnorm is used after every convolutional layer except the last one. Each layer uses LeakyReLU with slope of 0.2 for each layer except the last which has no activation function (when no activation function is specified the default activation is linear).

<img src="https://user-images.githubusercontent.com/25568287/184556222-1acf5ef8-8392-4feb-a0d2-b73fc3ccd0cf.png" width="500" />

###### Discriminator
The discriminator takes an input image size of 256 x 256 and returns one output value. Its main objective is to classify the input images as real or fake by trying to minimise its loss. Batchnorm is used after every convolutional layer except the last one. I also used dropout layers with a dropout of 0.4. Each layer uses LeakyReLU with slope of 0.2 for each layer except the last which has no activation function (when no activation function is specified the default activation is linear).

<img src="https://user-images.githubusercontent.com/25568287/184556225-e0ef77d2-078b-4cf2-a89f-71c57ebb5720.png" width="500" />

##### Loss functions and optimisers
The loss for the discriminator was defined as the sum of the binary crossentropy of classifying the real images and the binary crossentropy of classifying the fake images. Which means that loss is minimised for the discriminator if it is able to correctly classify real images as real and fake images as fake. The loss for the generator was defined as the binary crossentropy of fake images with real image labels. This means the loss for the generator is minimised if its generated images are classified as real. 

I used the Adam optimiser with learning rate 0.0002 for the generator and a learning rate of 0.0001 for the discriminator as I found the generator loss was not growing as quickly as I would have liked when the discriminator learning rate was 0.0002.

##### Generator and discriminator losses
Here is a plot of the generator(blue) and discriminator(yellow) losses for the 500 epochs. At around 25 epochs the training appears to stabilise and converge.

![loss Graph](https://user-images.githubusercontent.com/25568287/185814184-f8fe7d21-9187-44d9-9419-380399a4c2d3.png)

##### SSIM
After generating images from the trained generator, you can choose an image to calculate the SSIM for. Only one image is chosen since the calculation involves iterating over the entire training set and calculating and storing the SSIM value which is computational expensive as there are almost over 600 training images. The maximum SSIM is then displayed along with the corresponding training image which is closest in structural similarity to the generated image. With 500 epochs, the SSIM should be above 0.75 with some images reaching 0.8.

The following example was generated after 500 epochs with an SSIM of 0.75.

![image](https://user-images.githubusercontent.com/25568287/184556354-ddc909bc-9094-48f0-95ef-5c77d2e658bd.png)

> #### Step 5. Classification Phase
##### Dataset details

Training dataset | LUAD | LUSC
--- | --- | --- |
Orignal | 385  | 385 |
Fake | 1050  | 1050 |
Total | 1435  | 1435 |

Testing dataset | LUAD | LUSC
--- | --- | --- |
Orignal | 209  | 160 |
Fake | 450  | 450 |
Total | 659  | 610 |

##### Model details
We started designing `ResNet50` Network

![Res50 (1)](https://user-images.githubusercontent.com/25568287/189000216-da3133b6-54da-4fe4-9fa6-f22ea537867c.png)

##### Loss and accuracy
Loss = `0.00013833338053903757`
Test Accuracy = `97 %`

> #### Step 6. Attribution Method using `Integrated Gradients`
At this stage, which is one of the last stages we have in this hypothesis, which is called `Attribution method` that is refer to an explainable AI technique introduced in the paper Axiomatic Attribution for Deep Networks. Recently has come to apply this concept very clearly and on a new scale that is called `Integrated Gradients` as IG. `IG` aims to explain the relationship between a model's predictions in terms of its features. It has many use cases including understanding feature importances, identifying data skew, and debugging model performance. IG has become a popular interpretability technique due to its broad applicability to any differentiable model (e.g. images, text, structured data), ease of implementation, theoretical justifications, and computational efficiency relative to alternative approaches that allows it to scale to large networks and feature spaces such as images.

Using keras `backend` utils can we resolve and explain our model in previos phase:

``` python
def explain(self, sample, outc=0, reference=False, num_steps=50, verbose=0):
        
        # Each element for each input stream.
        samples = []
        numsteps = []
        step_sizes = []
        
        # If multiple inputs are present, feed them as list of np arrays. 
        if isinstance(sample, list):
            #If reference is present, reference and sample size need to be equal.
            if reference != False: 
                assert len(sample) == len(reference)
            for i in range(len(sample)):
                if reference == False:
                    _output = integrated_gradients.linearly_interpolate(sample[i], False, num_steps)
                else:
                    _output = integrated_gradients.linearly_interpolate(sample[i], reference[i], num_steps)
                samples.append(_output[0])
                numsteps.append(_output[1])
                step_sizes.append(_output[2])
        
        # Or you can feed just a single numpy arrray. 
        elif isinstance(sample, np.ndarray):
            _output = integrated_gradients.linearly_interpolate(sample, reference, num_steps)
            samples.append(_output[0])
            numsteps.append(_output[1])
            step_sizes.append(_output[2])
            
        # Desired channel must be in the list of outputchannels
        assert outc in self.outchannels
        if verbose: print("Explaning the "+str(self.outchannels[outc])+"th output.")
            
        # For tensorflow backend
        _input = []
        for s in samples:
            _input.append(s)
        _input.append(0)
        
        if K.backend() == "tensorflow": 
            gradients = self.get_gradients[outc](_input)
        elif K.backend() == "theano":
            gradients = self.get_gradients[outc](_input)
            if len(self.model.inputs) == 1:
                gradients = [gradients]
        
        explanation = []
        for i in range(len(gradients)):
            _temp = np.sum(gradients[i], axis=0)
            explanation.append(np.multiply(_temp, step_sizes[i]))
           
        # Format the return values according to the input sample.
        if isinstance(sample, list):
            return explanation
        elif isinstance(sample, np.ndarray):
            return explanation[0]
        return -1
```

##### Calculate Integrated Gradients
Our model mapping between your input feature space, image pixel values, and an output space defined by LUNG class probability values between 0 (LUAD) and 1 (LUSC). Early interpretability methods for neural networks assigned feature importance scores using gradients, which tell you which pixels have the steepest local relative to your model's prediction at a given point along your model's prediction function. However, gradients only describe local changes in your model's prediction function with respect to pixel values and do not fully describe your entire model prediction function.

The formula for Integrated Gradients is as follows:

![Screenshot_8](https://user-images.githubusercontent.com/25568287/191078067-b1d1cfe8-6e71-4342-9855-d4ad212d5c8c.png)

`where`
- i  = feature (individual pixel)
- x  = input (image tensor)
- x′  = baseline (image tensor)
- k  = scaled feature perturbation constant
- m  = number of steps in the Riemann sum approximation of the integral
- (xi−x′i)  = a term for the difference from the baseline. This is necessary to scale the integrated gradients and keep them in terms of the original image. The path from the baseline image to the input is in pixel space. Since with IG you are integrating in a straight line (linear transformation) this ends up being roughly equivalent to the integral term of the derivative of the interpolated image function with respect to  α  with enough steps. The integral sums each pixel's gradient times the change in the pixel along the path. It's simpler to implement this integration as uniform steps from one image to the other, substituting  x:=(x′+α(x−x′)) . - dx=(x−x′)dα . The (x−x′) term is constant and is factored out of the integral.

Samples from our output after attribution method phase after explain our `ResNet50` model:

<p float="center">
  <img src="https://user-images.githubusercontent.com/25568287/191091297-51b83440-5276-4b3b-9f72-1135da44b2c1.png" width="170" />
  <img src="https://user-images.githubusercontent.com/25568287/191091401-edee5c13-edad-4e03-b55b-64b732bfce81.png" width="170" />
  <img src="https://user-images.githubusercontent.com/25568287/191091949-ffd2eeaa-c65d-44be-b112-e363e480e22b.png" width="170" />
  <img src="https://user-images.githubusercontent.com/25568287/191091956-bf3e98cf-fa0e-4dae-81ef-110251f86955.png" width="170" />
  <img src="https://user-images.githubusercontent.com/25568287/191091962-f62fece0-0e4c-4e6a-ad3b-33b14e3f5714.png" width="170" />
  <img src="https://user-images.githubusercontent.com/25568287/191091971-a14d87c8-1ca3-4a97-8523-9b91e8120702.png" width="170" />
  <img src="https://user-images.githubusercontent.com/25568287/191091980-a68bff43-0334-40c9-a53d-58da29282dff.png" width="170" />
  <img src="https://user-images.githubusercontent.com/25568287/191091987-746d1119-df54-478d-84ba-bf64edde85eb.png" width="170" />
  <img src="https://user-images.githubusercontent.com/25568287/191091995-ae75b709-64a9-4987-9d8c-87583f4763b9.png" width="170" />
  <img src="https://user-images.githubusercontent.com/25568287/191092004-0ce74bfb-1bf1-4b25-8f11-f581bad75a5f.png" width="170" />
</p>

Previous results show more the PXs that in the end represents the group of `Genes` that lead the end to the occurrence of lung cancer.

> #### Step 7. Localize max weight

The last phase in our hypothesis that we will process localize operation by max weight for get most of genes that lead to occur `Lung cancer`

## License
[MIT](https://choosealicense.com/licenses/mit/)
